local M = {}

M.EMBOSS_COLOR = {
	highlight = { r = 32 / 255, g = 70 / 255, b = 9 / 255 },
	shadow = { r = 32 / 255, g = 70 / 255, b = 9 / 255 }
}

function M.background( background )
	-- create background
	local o = display.newImageRect( background or 'images/bg/grass.jpg', screenW + 10, screenH + 10 )

	o.x = halfW
	o.y = halfH

	return o
end

function M.buildHID( sceneGroup )
	-- casomiera
	local clock = display.newImageRect( sceneGroup, 'images/replay.png', 32, 32 )
	clock.anchorX = 0
	clock.anchorY = 0
	clock.x = 5
	clock.y = 5

	local duration = display.newEmbossedText( {
		text='',
		font = 'GoodDogPlain',
		fontSize=30,
		x=42,
		y=5
	} )
	duration.anchorX = 0
	duration.anchorY = 0
	duration:setEmbossColor( M.EMBOSS_COLOR )
	sceneGroup:insert( duration )

	-- score
	local coin = display.newImageRect( sceneGroup, 'images/coin.png', 32, 32 )
	coin.anchorX = 0
	coin.anchorY = 0
	coin.x = -100
	coin.y = 5

	local score = display.newEmbossedText( {
		text='',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = -100,
		y = 5
	} )
	score.anchorX = 0
	score.anchorY = 0
	score:setEmbossColor( M.EMBOSS_COLOR )
	sceneGroup:insert( score )

	-- pause button
	local pause_group = display.newGroup( )
	pause_group.alpha = 0	
	sceneGroup:insert( pause_group )

	local pause_button = display.newImageRect( pause_group, 'images/pause.png', 32, 32 )
	pause_button.x = halfW - 30
	pause_button.y = 22

	local pause_label = display.newEmbossedText( {
		text = 'Pause',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW - 5,
		y = 22
	} )
	pause_label.anchorX = 0
	pause_label:setEmbossColor( M.EMBOSS_COLOR )
	pause_group:insert( pause_label )

	return duration, score, coin, pause_group
end

function M.tapToPlay()
	local text = display.newEmbossedText( {
		text = 'Tap to play...',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW,
		y = screenH - 24
	} )
	text:setEmbossColor( M.EMBOSS_COLOR )

	return text
end

function M.levelNumber( sceneGroup )
	local text = display.newEmbossedText( {
		text = '', font = 'GoodDogPlain', fontSize = 30, x = halfW, y = 120
	} )
	text:setEmbossColor( M.EMBOSS_COLOR )

	sceneGroup:insert( text )

	return text
end

function M.levelDescription( sceneGroup )
	local text = display.newEmbossedText( {
		text = '', font = 'GoodDogPlain', fontSize = 30, x = halfW, y = screenH - 100, width = screenW, align = 'center'
	} )
	text:setEmbossColor( M.EMBOSS_COLOR )

	sceneGroup:insert( text )

	return text
end

function M.audioSwitchIcon()
	local audio_group = display.newGroup( )

	local imageSheetOptions = {
		width = 60,
		height = 60,
		numFrames = 2,
		sheetContentWidth = 120,
		sheetContentHeight = 60
	}

	local sheet = graphics.newImageSheet( 'images/audio.png', imageSheetOptions)
	
	local o = display.newSprite( sheet, {
		{name = 'on', start = 1, count = 1},
		{name = 'off', start = 2, count = 1}
	} )
	audio_group:insert( o )

	o.x = halfW - 40
	o.y = 160
	
	local audio_label = display.newEmbossedText( {
		text = 'Sound',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW - 5,
		y = 160
	} )
	audio_label.anchorX = 0
	audio_label:setEmbossColor( M.EMBOSS_COLOR )
	audio_group:insert( audio_label )

	audio_group.setSequence = function ( name )
		o:setSequence( name )
	end

	audio_group.play = function ( name )
		o:play( name )
	end

	return audio_group
end

function M.musicSwitchIcon()
	local music_group = display.newGroup( )

	local imageSheetOptions = {
		width = 60,
		height = 60,
		numFrames = 2,
		sheetContentWidth = 120,
		sheetContentHeight = 60
	}

	local sheet = graphics.newImageSheet( 'images/music.png', imageSheetOptions)
	
	local o = display.newSprite( sheet, {
		{name = 'on', start = 1, count = 1},
		{name = 'off', start = 2, count = 1}
	} )
	music_group:insert( o )

	o.x = halfW - 40
	o.y = 220

	local music_label = display.newEmbossedText( {
		text = 'Music',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW - 5,
		y = 220
	} )
	music_label.anchorX = 0
	music_label:setEmbossColor( M.EMBOSS_COLOR )
	music_group:insert( music_label )

	music_group.setSequence = function ( name )
		o:setSequence( name )
	end

	music_group.play = function ( name )
		o:play( name )
	end

	return music_group
end

function M.leaderBoardIcon()
	local o = display.newImageRect( 'images/leaderboard.png', 60, 60 )

	o.x = halfW - 100
	o.y = screenH - 20
	o.anchorY = 1

	return o
end

function M.button_down( button )
	button.y = button.y + 2
	button.down = true

	display.currentStage:setFocus( button )

	sounds.play(sounds.sound.button_click)
end

function M.button_up( button )
	if button.down then
		button.y = button.y - 2
		button.down = false

		display.currentStage:setFocus( nil )

		sounds.play(sounds.sound.button_click)

		return true
	else
		return false
	end
end

function M.social_networks_buttons( parent )
	-- facebook
	local facebook_button = display.newImageRect( parent, 'images/facebook.png', 32, 32 )
	facebook_button.alpha = 0.9
	facebook_button.x = screenW - 16 - 6
	facebook_button.y = halfH - 20

	utils.handlerAdd( facebook_button, 'touch', function ( self, event )
		if event.phase == 'began' then
			M.button_down( facebook_button )
		elseif event.phase == 'ended' and M.button_up( facebook_button ) then
			system.openURL( R.strings('facebook_url') )
		end

		return true
	end )

	-- twitter
	local twitter_button = display.newImageRect( parent, 'images/twitter.png', 32, 32 )
	twitter_button.alpha = 0.9
	twitter_button.x = screenW - 16 - 6
	twitter_button.y = halfH + 20

	utils.handlerAdd( twitter_button, 'touch', function ( self, event )
		if event.phase == 'began' then
			M.button_down( twitter_button )
		elseif event.phase == 'ended' and M.button_up( twitter_button ) then
			system.openURL( R.strings('twitter_url') )
		end

		return true
	end )
end

function M.health_indicator( parent_group, effects_group )
	local indicator_group = display.newGroup( )
	parent_group:insert( indicator_group )

	local hearth = display.newImageRect( indicator_group, 'images/hearth.png', 32, 32 )
	hearth.x = halfW - 22
	hearth.y = screenH - 22

	local indicator_text = display.newEmbossedText( {
		text='',
		font = 'GoodDogPlain',
		fontSize = 28,
		x = halfW - 4,
		y = screenH - 22
	} )
	indicator_text.anchorX = 0
	indicator_text:setEmbossColor( M.EMBOSS_COLOR )
	indicator_group:insert( indicator_text )

	indicator_group.update = function( health, effect )
		if indicator_group.last_health == health then return end

		indicator_group.last_health = health
		indicator_text.text = health .. '%'

		if health < 10 then
			indicator_text:setFillColor( 1, 0, 0 )
		else
			indicator_text:setFillColor( 1, 1, 1 )
		end

		indicator_text:setEmbossColor( M.EMBOSS_COLOR )

		if effect then
			local health_effect = display.newText( {
				text = indicator_text.text,
				font = 'GoodDogPlain',
				fontSize = 18,
				x = indicator_text.x + (indicator_text.width * 0.5),
				y = indicator_text.y,
				alpha = 0.5,
			} )
			if health < 10 then
				health_effect:setFillColor( 1, 0, 0 )
			end
			
			indicator_group.effects_group:insert(health_effect)

			transition.to( health_effect, {
				xScale = 4,
				yScale = 4,
				alpha = 0,
				time = 400,
				onComplete = function ( ... )
					health_effect:removeSelf( )
					health_effect = nil
				end
			} )
		end
	end

	return indicator_group
end

return M