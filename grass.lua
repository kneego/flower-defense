local table = require( 'table' )
local math = require( 'math' )

local _M = {}

local _shift = 10
local _speed = 2000

local function moveFinished( o )
	if o.moveDirection == 'L' then
		o.moveDirection = 'R'
		moveBy = _shift * 2
	else
		o.moveDirection = 'L'
		moveBy = -(_shift * 2)
	end

	o.transition = transition.moveBy( o, {
		x = moveBy, time = _speed, onComplete = moveFinished
	} )
end

function _M.init()
	return display.newGroup( )
end

function _M.show(group)
	local _grass = {}

	local imageSheetOptions = {
		frames = {
			{x = 0, y = 0, width = 640, height = 222},
			{x = 0, y = 223, width = 640, height = 232},
			{x = 0, y = 457, width = 640, height = 228},
			{x = 0, y = 686, width = 640, height = 226},
			{x = 0, y = 914, width = 640, height = 226},
		},
		
		sheetContentWidth = 640,
		sheetContentHeight = 1141
	}

	local imageSheet = graphics.newImageSheet( 'images/bg/grass.png', imageSheetOptions )

	local grassDefinition = {
		{frame = 5, x = halfW, y = 0, width = 640 * 0.6, height = 226 * 0.6, direction = 'L', initShift = 10},
		{frame = 1, x = halfW, y = 50, width = 640 * 0.6, height = 223 * 0.6, direction = 'L', initShift = 8},
		{frame = 2, x = halfW, y = 100, width = 640 * 0.6, height = 234 * 0.6, direction = 'L', initShift = 6},
		{frame = 4, x = halfW, y = 150, width = 640 * 0.6, height = 229 * 0.6, direction = 'L', initShift = 4},
		{frame = 5, x = halfW, y = 200, width = 640 * 0.6, height = 228 * 0.6, direction = 'L', initShift = 2},
		{frame = 1, x = halfW, y = 250, width = 640 * 0.6, height = 226 * 0.6, direction = 'L', initShift = 0},
		{frame = 2, x = halfW, y = 300, width = 640 * 0.6, height = 223 * 0.6, direction = 'L', initShift = -2},
		{frame = 4, x = halfW, y = 350, width = 640 * 0.6, height = 234 * 0.6, direction = 'L', initShift = -4},
		{frame = 5, x = halfW, y = 400, width = 640 * 0.6, height = 229 * 0.6, direction = 'L', initShift = -6},
		{frame = 1, x = halfW, y = 450, width = 640 * 0.6, height = 228 * 0.6, direction = 'L', initShift = -8},
		{frame = 2, x = halfW, y = 500, width = 640 * 0.6, height = 226 * 0.6, direction = 'L', initShift = -10},
		{frame = 4, x = halfW, y = 550, width = 640 * 0.6, height = 223 * 0.6, direction = 'L', initShift = -8},
		{frame = 5, x = halfW, y = 600, width = 640 * 0.6, height = 234 * 0.6, direction = 'L', initShift = -6},
	}

	for i = 1, #grassDefinition do
		local definition = grassDefinition[i]

		local o = display.newImage( imageSheet, definition['frame'] )
		o.x = definition['x'] + definition['initShift']
		o.y = definition['y']
		o.width = definition['width']
		o.height = definition['height']
		o.moveDirection = definition['direction']
		o.initShift = definition['initShift']
		
		table.insert( _grass, o )

		group:insert(o)
	end

	group.grass = _grass
	return group
end

function _M.start(group)
	for i = 1, #group.grass do
		o = group.grass[i]
		if o.moveDirection == 'L' then
			moveBy = -(_shift) - o.initShift
		else
			moveBy = _shift - o.initShift
		end

		o.transition = transition.moveBy( o, {
			x = moveBy, time = math.abs(moveBy * (_speed / (_shift * 2))), onComplete = moveFinished
		} )
	end
end

function _M.remove(group)
	for i = 1, #group.grass do
		transition.cancel(group.grass[i].transtition)
		group.grass[i]:removeSelf()
	end
end

function _M.pause(group)
	for i = 1, #group.grass do
		transition.pause(group.grass[i].transtition)
	end
end

function _M.resume(group)
	for i = 1, #group.grass do
		transition.resume(group.grass[i].transtition)
	end
end

return _M