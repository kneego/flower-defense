local composer = require( "composer" )
local scene = composer.newScene()

local native = require( "native" )
local widget = require( "widget" )
local json = require( "json" )

local common = require( "common" )
local levels = require( "levels" )
local levelmenu = require( "knee.levelmenu" )

local bg
local level_menu_group

function scene:create( event )
	utils.debug('level_selection:scene:create')

	-- initialize the scene
	local sceneGroup = self.view

	bg = common.background()
	sceneGroup:insert( bg )

	local bug = display.newImageRect( sceneGroup, 'images/bg/bug.png', screenW * display_ar, screenH )
	bug.x = halfW
	bug.y = halfH

	level_menu_group = display.newGroup( )
	sceneGroup:insert( level_menu_group )

	-- menu button
	local menu_group = display.newGroup( )
	sceneGroup:insert( menu_group )

	local menu_button = display.newImageRect( menu_group, 'images/menu.png', 54, 54 )
	menu_button.x = halfW - 40
	menu_button.y = screenH - 40

	local menu_label = display.newEmbossedText( {
		text = 'Main menu',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW - 5,
		y = screenH - 40
	} )
	menu_label.anchorX = 0
	menu_label:setEmbossColor( common.EMBOSS_COLOR )
	menu_group:insert( menu_label )

	utils.handlerAdd(menu_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( menu_group )
		elseif event.phase == 'ended' then
			common.button_up( menu_group )

			composer.gotoScene( 'menu' )
		end

		return true
	end )
end

function scene:show( event )
	if event.phase == 'did' then
		-- prepare levels definition
		levels_settings = settings.levels or {}
		
		for i = 1, #levels.levels do
			levels.levels[i].stars = (levels_settings[tostring(i)] or {}).stars or 0
		end

		-- create menu
		menu = levelmenu.createMenu({
			rows = 4,
			columns = 3,

			paddingLeft = 20,
			paddingRight = 20,
			paddingBottom = 90,
			padding = 30,

			itemWidth = 70,
			itemHeight = 70,

			levels = levels.levels,
			levelCount = #levels.levels,

			appendLevelNumber = true,

			font = 'GoodDogPlain',
			fontSize = 40,
			fontColor = { 1, 1, 1 },
			embossColor = common.EMBOSS_COLOR,

			imageLocked = 'images/locked.png',
			imageUnlocked = 'images/unlocked.png',
			imageStar = 'images/star.png',

			starOffset = 23,
			starSpace = 3,

			unlockedTo = settings.unlockedTo,

			eventListener = function ( self, event )
				if event.phase == 'began' then
					common.button_down( self )
				elseif event.phase == 'ended' then
					common.button_up( self )
				
					composer.gotoScene( 'game', {
						params = {
							level = event.target.levelNumber,
							score = 0
						}
					} )
				end

				return true
			end
		})

		level_menu_group:insert( menu )
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- pause the scene (stop timers, stop animation, unload sounds, etc.)
		menu:removeSelf( )
	elseif phase == "did" then
		
	end	
	
end

function scene:destroy( event )
	local sceneGroup = self.view

end

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene