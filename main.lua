require( 'knee.globals' )
require( 'common' )

-- data = require( "data" )
ads = require( 'ads' )
utils = require( 'knee.utils' )
back_button = require( 'knee.back_button' )

display.setStatusBar( display.HiddenStatusBar )
display.setDefault( 'background', 0, 0, 0 )
display.setDefault( 'fillColor', utils.color(255, 255, 255) )

debugging = true

if debugging then
	require( 'knee.screen_capture' )
end

game_network = require( 'knee.game_network' )
game_network.debugging = true

game_network.load_local_player_callback = function (event)
	if event ~= nil and event.data ~= nil then
		infinario:identify( event.data.playerID )

		infinario:update({
			alias = event.data.alias,
		})
	end
end

game_network.init()

-- rate dialog
rate = require( 'knee.rate' )
rate.init({
	android_rate = 'market://details?id=org.kneego.flower_defense',
	ios_rate = 'itms://itunes.apple.com/us/app/flower-defense/id974980772?mt=8',
	times_used = 5,
	days_used = 10,
	version = app_version,
	remind_times = 5,
	remind_days = 10,
	rate_title = 'Rate Flower Defense',
	rate_text = 'If you enjoy this game, please take a moment to rate it. Thank you for your support!',
	rate_button = 'Rate now',
	remind_button = 'Remind later',
	cancel_button = 'No, thanks',
})

-- resources (strings, arrays)
R = require( 'resources' )
sounds = require('sounds')

ga = require( 'knee.ga' )
ga:init( 'UA-39791763-23' )

-- reklamu zobraujeme inu pre iOS a inu pre Android
ads.init( 'admob', R.arrays('admob_id')[platform_name])

back_button.init()

-- load settings
settings = utils.loadSettings({
	-- default empty data
	emptyData = {
		unlockedTo = 1,
		sounds = true,
		music = true,
		show_help = true,
		game_play_counter = 0,
		levels = {},
	}
})

-- load menu screen
local composer = require( 'composer' )

composer.gotoScene( 'menu' )