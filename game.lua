local composer = require( "composer" )
local scene = composer.newScene()

local physics = require( "physics" )
local widget = require( "widget" )
local timer = require( "timer" )
local transition = require( "transition" )
local native = require( "native" )
local json = require( "json" )
local table = require( "table" )

local common = require( "common" )
local bug = require( "bug" )
local levels = require( "levels" )
local particles = require( 'knee.particles' )
local shake = require( 'knee.shake' )

-- flowers
local flower = require( "flower" )
local sunflower = require('flower.sunflower')

local effects_master_group
local effects_group

-- score
local score

-- game speed will increase every second
local gameSpeed

-- bugs array (for resource cleaning)
local bugs

-- HID
local health_indicator
local scoreText
local pauseBtn
local coin

local duration -- text object
local level_duration -- current number

local bg_group -- here we put background
local splash_group -- here we put all splash
local splash_master_group -- here we put all splash
local hid_group
local bug_group
local pause_group
local pause_bg_group

local bg -- background
local pause_bg -- pause background
local levelDescription -- Ant attack!
local levelNumber -- Prepare for level 1

local flowerObject -- will handle flower object
local flower_ghost
local flower_win
local bugGroup -- put all bugs in to this group
local bugCount -- number of bugs on screen
local gameIsRunning -- create new bug only if this is true

local timerBugEmit
local timerFlowerHealth


function show_ads()
	if ((settings.game_play_counter or 0) % 3 == 0) or show_ad then
		show_ad = true
		if ads.isLoaded("interstitial") then
			show_ad = false
			ads.show( "interstitial", { x=0, y=0, testMode = false } )
		end
	end
end

function stopGame( ads )
	-- show ads
	if ads == nil then ads = true end
	if ads then
		show_ads()
	end

	-- pause button skovame
	pauseBtn.alpha = 0

	-- stop creating new bugs
	gameIsRunning = false

	if timerBugEmit ~= nil then timer.cancel( timerBugEmit ) end
	if timerFlowerHealth ~= nil then timer.cancel( timerFlowerHealth ) end
	if game_timer ~= nil then timer.cancel( game_timer ) end

	-- stop all timers
	if bugGroup.numChildren ~= nil then
		for i = 1, bugGroup.numChildren do
			if bugGroup[i].timer ~= nil then
				timer.cancel( bugGroup[i].timer )
				bugGroup[i].timer = nil
			end
		end
	end

	timerBugEmit = nil
	timerFlowerHealth = nil
	game_timer = nil

	shake.stop()
end

local function game_timer_handler( event )
	gameSpeed = gameSpeed + levels.levels[level].speed

	-- kontrola casovaca
	level_duration = level_duration - 1

	if level_duration < 0 then
		-- end of the game
		pauseGame()
		stopGame()

		if level + 1 > settings.unlockedTo then
			settings.unlockedTo = level + 1
		end

		-- upravime pocet hviezdiciek
		local levels_settings = settings.levels or {}
		local current_stars = (levels_settings[tostring(level)] or {}).stars or 0

		utils.debug('current_stars', current_stars)

		local new_stars
		if flowerObject.health > 90 then
			new_stars = 3
		elseif flowerObject.health > 70 then
			new_stars = 2
		elseif flowerObject.health > 40 then
			new_stars = 1
		else
			new_stars = 0
		end

		if new_stars > current_stars then
			if settings.levels[tostring(level)] == nil then
				settings.levels[tostring(level)] = {}
			end

			settings.levels[tostring(level)].stars = new_stars
		end

		utils.saveSettings( settings )

		-- fanfara
		audio.fadeOut({ channel = sounds.channel.MUSIC_CHANNEL, time = 200 })
		sounds.play( sounds.sound.success, sounds.channel.SFX_CHANNEL )

		-- animacia kvetiny
		flower_win.alpha = 0.6
		transition.to( flower_win, {
			xScale = 2,
			yScale = 2,
			alpha = 1,
			time = 1500,
			transition = easing.inQuart,
			onComplete = function ( ... )
				composer.gotoScene( 'win', {
					params = {
						currentLevel = level,
						nextLevel = level + 1,
						score = score
					}
				} )
			end
		} )
	else
		updateHID()
	end
end

local function bugRubbishComplete( obj )
	if obj and obj.removeSelf then
		obj:removeSelf( )
	end
end

local function bugRubbishRemove( event )
	transition.to( event.source.params.obj, { alpha = 0, time = 1000, onComplete = bugRubbishComplete } )
end

local function bugTouchListener( self, event )
	-- utils.debug('game:bugTouchListener')

	if event.phase == 'began' then
		self.life = self.life - 1

		if math.random( 100 ) > 50 then
			-- make sound
			sounds.play(sounds.sound.splatch1)
		else
			sounds.play(sounds.sound.splatch2)
		end

		-- bug flash
		-- TODO
		
		if self.life < 1 then
			-- remove listeners
			utils.handlerRemove(self, 'touch')
			utils.handlerRemove(self, 'collision')

			-- update counters
			score = score + self.score
			bugCount = bugCount - 1

			-- score effect
			local score_effect = display.newText( {
				text = '+' .. self.score,
				font = 'GoodDogPlain',
				fontSize = 18,
				x = self.x,
				y = self.y,
			} )
			effects_group:insert(score_effect)

			transition.to( score_effect, {
				xScale = 4,
				yScale = 4,
				time = 700,
				y = self.y - 25,
				onComplete = function ( ... )
					score_effect:removeSelf( )
					score_effect = nil
				end
			} )

			-- stop eating handler
			if self.timer ~= nil then
				timer.cancel( self.timer )
				self.timer = nil
			end

			-- change image
			self:setSequence( 'dead' )
			self:play()

			-- stop moving
			self.stopMoving()

			t = timer.performWithDelay( 500, bugRubbishRemove )
			t.params = {
				obj = self
			}

			updateHID()

			-- show splash animation
			local splash = self.splash()
			splash_group:insert( splash )

			-- bigger particles
			local emitter = particles.get_emiter('particles/green-big.json')
			if self.add_to_group == nil then
				emitter.x = self.x
				emitter.y = self.y	
			else
				self.add_to_group( emitter )
			end

			-- shake effect
			if self.shake then
				shake.start()
				timer.performWithDelay( 400, function ( ... )
					shake.stop()
				end)
			end
		else
			self.pause_move()
			
			-- small particles
			local emitter = particles.get_emiter('particles/green.json')
			if self.add_to_group == nil then
				emitter.x = self.x
				emitter.y = self.y	
			else
				self.add_to_group( emitter )
			end
		end
	end

	return true
end

local function bugEatingHandler( event )
	-- utils.debug('game:bugEatingHandler')
	
	flowerObject.health = flowerObject.health - event.source.params.power

	if flowerObject.health < 0 then
		flowerObject.health = 0
	end

	-- update HID
	updateHID()
end

local function bugFinalize( self, event )
	-- utils.debug('game:bugFinalize')

	if self.timer ~= nil then
		timer.cancel( self.timer )
		self.timer = nil
	end
end

local function bugCollisionHandler( self, event )
	-- utils.debug('game:bugCollisionHandler')

	if event.phase == 'began' then
		transition.cancel( self )
		self:removeEventListener( 'collision', bugCollisionHandler )
		self:setSequence( 'eat' )
		self:play()
		
		timer.performWithDelay( 1, function ( event )
			self.isBodyActive = false
		end )

		self.timer = timer.performWithDelay( 500, bugEatingHandler, 0 )
		self.timer.params = {
			power = self.power
		}
	end
end

local function bugEmitHandler( event )
	-- utils.debug('game:bugEmit')

	local weight = 0
	local newBug

	local randomWeight = math.random( 100 )

	local currentLevel = levels.levels[level]

	-- calculate next delay
	delay = 1500 - (( 800 * math.log( 1 + gameSpeed / 10) ) / math.log( 10 ))
	if delay < 100 then delay = 100 end

	-- if we have enough bugs
	if currentLevel.maxBugsCount and bugCount >= currentLevel.maxBugsCount then 
		timerBugEmit = timer.performWithDelay( delay, bugEmitHandler)
		return
	end

	-- find right bug for emiting
	for i = 1, #currentLevel.bugs do
		local bugDefinition = currentLevel.bugs[i]

		if randomWeight >= weight and randomWeight < weight + bugDefinition.bugWeight then
			-- we found bug
			newBug = bug[bugDefinition.bugName]()
			break
		else
			weight = weight + bugDefinition.bugWeight
		end
	end

	if gameIsRunning and newBug then
		-- emit one bug
		newBug:setSequence( 'walk' )
		newBug:play()

		utils.handlerAdd(newBug, 'touch', bugTouchListener)
		utils.handlerAdd(newBug, 'collision', bugCollisionHandler)
		utils.handlerAdd(newBug, 'finalize', bugFinalize)

		bugGroup:insert( newBug )

		physics.addBody( newBug, 'dynamic', { bounce = 0, filter = { categoryBits = 2, maskBits = 1 } } )

		newBug.move()

		bugCount = bugCount + 1

		table.insert(bugs, newBug)
	end

	timerBugEmit = timer.performWithDelay( delay, bugEmitHandler)
end

function flowerHealthTimerHandler( event )
	-- utils.debug('game:flowerHealthTimerHandler')

	if flowerObject.health <= 0 then
		pauseGame()
		stopGame()

		-- zastavime rotovanie kvetiny
		sunflower.pause( flowerObject )

		-- fail sound
		audio.fadeOut({ channel = sounds.channel.MUSIC_CHANNEL, time = 200 })
		sounds.play( sounds.sound.fail, sounds.channel.SFX_CHANNEL )

		-- animacia mrtvej kvetiny
		flower_ghost.alpha = 0.8
		transition.to( flower_ghost, {
			y = 0,
			alpha = 0,
			time = 2000,
			onComplete = function ( ... )
				composer.gotoScene( 'game_over', {
					params = {
						currentLevel = level,
						score = score
					}
				} )
			end
		} )
	else
		-- increase health
		if flowerObject.health < 100 then
			flowerObject.health = flowerObject.health + 1
		end

		-- update HID
		updateHID()
	end
end

function updateHID( effect )
	if effect == nil then effect = true end

	local health = flowerObject.health
	
	-- update health indicator
	health_indicator.update( health, effect )

	-- update flower face
	flowerObject.update_face( health )
	
	-- update score
	if tonumber(scoreText.text) ~= score then
		scoreText.text = score

		-- update score position
		scoreText.x = screenW - 10 - string.len(scoreText.text) * 10

		-- update coin position
		coin.x = screenW - 10 - string.len(scoreText.text) * 10 - 32 - 5

		if effect then
			local scoreEffect = display.newText( {
				text=score,
				font='GoodDogPlain',
				fontSize=30,
				x = scoreText.x + (scoreText.width * 0.5),
				y = scoreText.y + (scoreText.height * 0.5),
				alpha=0.5,
			} )
			effects_group:insert(scoreEffect)

			transition.to( scoreEffect, {
				xScale=4,
				yScale=4,
				alpha=0,
				time=400,
				onComplete = function ( ... )
					scoreEffect:removeSelf( )
					scoreEffect = nil
				end
			} )
		end
	end

	-- timer
	if tonumber(duration.text) ~= level_duration then
		duration.text = level_duration

		if effect then
			local duration_effect = display.newText( {
				text=level_duration,
				font='GoodDogPlain',
				fontSize=30,
				x = duration.x + (duration.width * 0.5),
				y = duration.y + (duration.height * 0.5),
				alpha=0.5,
			} )
			effects_group:insert(duration_effect)

			transition.to( duration_effect, {
				xScale=4,
				yScale=4,
				alpha=0,
				time=400,
				onComplete = function ( ... )
					duration_effect:removeSelf( )
					duration_effect = nil
				end
			} )
		end
	end
end

function create_pause_group( parent )
	local pause_group = display.newGroup( )
	pause_group.alpha = 0
	parent:insert( pause_group )

	pause_bg_group = display.newGroup( )
	pause_bg_group.alpha = 0.7
	pause_group:insert( pause_bg_group )

	-- cez pauze groupu nemoze prejst ziadny touch event
	utils.handlerAdd(bg_group, 'touch', function ( ... )
		return true
	end)

	-- logo
	local flower_defence = display.newImageRect( pause_group, 'images/flower-defence.png', 220, 69 )
	flower_defence.x = halfW
	flower_defence.y = 60

	-- info o pauznuti
	local text = display.newEmbossedText( {
		text = 'Game Paused',
		font = 'GoodDogPlain',
		fontSize = 54,
		x = halfW,
		y = halfH - 40
	} )
	text:setEmbossColor( common.EMBOSS_COLOR )
	pause_group:insert( text )

	-- fb button

	-- twitter button

	-- resume button
	local resume_group = display.newGroup( )
	pause_group:insert( resume_group )

	local y = screenH - 110
	local resume_button = display.newImageRect( resume_group, 'images/play.png', 54, 54 )
	resume_button.x = halfW - 40
	resume_button.y = y

	local resume_label = display.newEmbossedText( {
		text = 'Resume',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW - 5,
		y = y
	} )
	resume_label.anchorX = 0
	resume_label:setEmbossColor( common.EMBOSS_COLOR )
	resume_group:insert( resume_label )

	utils.handlerAdd(resume_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( resume_group )
		elseif event.phase == 'ended' and common.button_up( resume_group ) then
			-- skovame pause groupu
			pause_group.alpha = 0

			-- a pustime hru dalej
			resumeGame()
		end

		return true
	end )

	-- main menu
	local menu_group = display.newGroup( )
	pause_group:insert( menu_group )

	local y = screenH - 50
	local menu_button = display.newImageRect( menu_group, 'images/menu.png', 54, 54 )
	menu_button.x = halfW - 40
	menu_button.y = y

	local menu_label = display.newEmbossedText( {
		text = 'Main menu',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW - 5,
		y = y
	} )
	menu_label.anchorX = 0
	menu_label:setEmbossColor( common.EMBOSS_COLOR )
	menu_group:insert( menu_label )

	utils.handlerAdd(menu_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( menu_group )
		elseif event.phase == 'ended' and common.button_up( menu_group ) then
			-- skovame pause groupu
			pause_group.alpha = 0

			-- a zastavime hru
			stopGame( false )

			composer.gotoScene( 'menu' )
		end

		return true
	end )

	return pause_group
end

function pauseGame()
	utils.debug('game:pause')

	-- stop creating new bugs
	gameIsRunning = false

	if timerBugEmit ~= nil then timer.pause( timerBugEmit ) end
	if timerFlowerHealth ~= nil then timer.pause( timerFlowerHealth ) end
	if game_timer ~= nil then timer.pause( game_timer ) end

	-- stop all timers
	if bugGroup.numChildren ~= nil then
		for i = 1, bugGroup.numChildren do
			if bugGroup[i].timer ~= nil then
				timer.pause( bugGroup[i].timer )
			end

			utils.handlerRemove(bugGroup[i], 'touch')
			bugGroup[i]:pause()
		end
	end

	sunflower.pause( flowerObject )
end

function resumeGame()
	utils.debug('game:resume')

	-- stop creating new bugs
	gameIsRunning = true

	if timerBugEmit ~= nil then timer.resume( timerBugEmit ) end
	if timerFlowerHealth ~= nil then timer.resume( timerFlowerHealth ) end
	if game_timer ~= nil then timer.resume( game_timer ) end

	-- stop all timers
	if bugGroup.numChildren ~= nil then
		for i = 1, bugGroup.numChildren do
			if bugGroup[i].timer ~= nil then
				timer.resume( bugGroup[i].timer )
			end

			utils.handlerAdd(bugGroup[i], 'touch', bugTouchListener)
			bugGroup[i]:play()
		end
	end

	sunflower.resume(flowerObject)
end

local function startGame()
	-- reklamu nechceme
	ads.hide()

	settings.game_play_counter = (settings.game_play_counter or 0) + 1
	utils.saveSettings( settings )

	levelNumber.text = ''
	levelDescription.text = ''

	-- allow to create new bug
	gameIsRunning = true

	timerBugEmit = timer.performWithDelay( 1500, bugEmitHandler)
	timerFlowerHealth = timer.performWithDelay( 800, flowerHealthTimerHandler, 0 )
	game_timer = timer.performWithDelay( 1000, game_timer_handler, 0 )

	-- zacala hra tak zobrazime pause button
	pauseBtn.alpha = 1

	-- naloadujeme reklamu
	ads.load( "interstitial", { testMode=false } )
end

function scene:create( event )
	utils.debug('game:scene:create')

	local sceneGroup = self.view

	bg_group = display.newGroup( )
	sceneGroup:insert( bg_group )

	splash_master_group = display.newGroup( )
	sceneGroup:insert( splash_master_group )

	flowerObject = sunflower.init()
	sceneGroup:insert( flowerObject )

	bug_master_group = display.newGroup( )
	sceneGroup:insert( bug_master_group )

	hid_group = display.newGroup( )
	sceneGroup:insert( hid_group)
	
	levelNumber = common.levelNumber( sceneGroup )
	levelDescription = common.levelDescription( sceneGroup )

	duration, scoreText, coin, pauseBtn = common.buildHID( hid_group )
	health_indicator = common.health_indicator( hid_group )

	-- pause button touch handler
	utils.handlerAdd(pauseBtn, 'touch', function( self, event )
		if event.phase == 'began' then
			common.button_down( pauseBtn )
		elseif event.phase == 'ended' and common.button_up( pauseBtn ) then
			-- zobrazime pause groupu
			pause_group.alpha = 1

			-- a pauzneme hru
			pauseGame( )
		end

		return true
	end)

	effects_master_group = display.newGroup( )
	sceneGroup:insert( effects_master_group )

	pause_group = create_pause_group( sceneGroup )

	-- duch sa zobrazi po strate zivota
	flower_ghost = display.newImageRect( sceneGroup, 'images/ghost.png', 150, 150 )

	-- toto je vyherny screen
	flower_win = display.newImageRect( sceneGroup, 'images/happy.png', 300, 300 )
	flower_win.x = halfW
	flower_win.y = halfH
end

function scene:show( event )
	utils.debug('game:scene:show', event.phase)

	local sceneGroup = self.view
	local phase = event.phase

	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
		gameSpeed = 0
		bugCount = 0
		bugs = {}
		
		if event.params and event.params.level then level = event.params.level else level = 1 end
		if event.params and event.params.score then score = event.params.score else score = 0 end

		-- add background
		bg = common.background( levels.levels[level].background )
		bg_group:insert( bg )

		-- add background in to pause group
		pause_bg = common.background( levels.levels[level].background )
		pause_bg_group:insert( pause_bg )

		flowerObject.health = levels.levels[level].flowerHealth
		level_duration = levels.levels[level].level_duration

		-- sem sa budu davat vsetky flaky
		splash_group = display.newGroup( )
		splash_master_group:insert( splash_group )

		-- sem pojdu chrobaky
		bugGroup = display.newGroup( )
		bug_master_group:insert( bugGroup )

		sunflower.show(flowerObject)

		-- grupa pre efekty score / health / ...
		effects_group = display.newGroup( )
		sceneGroup:insert( effects_master_group )

		health_indicator.effects_group = effects_group
		updateHID( false )

		-- pustime muziku
		if settings.music then
			audio.play( sounds.sound.music, {channel = sounds.channel.MUSIC_CHANNEL, loops = -1} )
			audio.setVolume( 0.15, {channel = sounds.channel.MUSIC_CHANNEL} )
		end

		-- pause groupa je skovana
		pause_group.alpha = 0

		-- default stav
		flower_win.xScale = 0.5
		flower_win.yScale = 0.5
		flower_win.alpha = 0

		flower_ghost.x = halfW
		flower_ghost.y = halfH
		flower_ghost.alpha = 0

	elseif phase == "did" then
		-- make the scene come alive (start timers, begin animation, play audio, etc.)
		physics.start()
		physics.setGravity( 0, 0 )

		physics.addBody( flowerObject.items[1], 'static', { bounce=0, radius=38, filter={ categoryBits=1, maskBits=2 } } )

		levelNumber.text = 'Prepare for level ' .. level

		if levels.levels[level].description then
			levelDescription.text = levels.levels[level].description
		end

		local starGameTimer = timer.performWithDelay( 5000,  function ( ... )
			utils.handlerRemove( bg, 'touch' )

			sunflower.start(flowerObject)
			startGame()
		end )

		utils.handlerAdd(bg, 'touch', function( self, event )
			if event.phase == 'began' then
				timer.cancel( starGameTimer )
				utils.handlerRemove( self, 'touch' )

				sunflower.start(flowerObject)
				startGame()
			end

			return true
		end )
	end
end

function scene:hide( event )
	utils.debug('game:scene:hide', event.phase)
	
	if event.phase == "will" then
		physics.stop( )

		bugGroup:removeSelf( )
		splash_group:removeSelf( )

		sunflower.remove(flowerObject)

		effects_group:removeSelf( )
		effects_group = nil

		if settings.music then
			audio.fadeOut({ channel = sounds.channel.MUSIC_CHANNEL, time = 1000 })
		end
	elseif event.phase == "did" then
		-- Called when the scene is now off screen
		bugGroup = nil

		bg:removeSelf( )
		bg = nil

		pause_bg:removeSelf( )
		pause_bg = nil

		shake.stop()
	end	
	
end

function scene:destroy( event )
	utils.debug('game:scene:destroy')
	
	package.loaded[physics] = nil
	physics = nil
end

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene