local _M = {}

function _M.flower()
	local o = display.newImage( 'images/flowers/flower.png' )
	o.x = halfW
	o.y = halfH
	o.width = 170
	o.height = 118

	o.defaultHealth = 10

	return o
end

return _M