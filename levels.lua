local _L = {}

_L.levels = {
	-- {speed = 1, maxBugsCount = 5, flowerHealth = 50, bugs = { 
	-- 	{bugName = 'ant', bugWeight = 5 },
	-- 	{bugName = 'fly', bugWeight = 5 },
	-- 	{bugName = 'snail', bugWeight = 10 },
	-- 	{bugName = 'chafer', bugWeight = 10 },
	-- 	{bugName = 'aphid', bugWeight = 10 },
	-- 	{bugName = 'lady', bugWeight = 10 },
	-- 	{bugName = 'earwig', bugWeight = 10 },
	-- 	{bugName = 'spider', bugWeight = 10 },
	-- 	{bugName = 'worm', bugWeight = 10 },
	-- 	{bugName = 'grasshopper', bugWeight = 10 },
	-- 	{bugName = 'mushi', bugWeight = 10 },
	-- }},

	{
		speed = 2, maxBugsCount = 4, flowerHealth = 100, level_duration = 30,
		bugs = { 
			{bugName = 'fly', bugWeight = 100 },
		},
		background = 'images/bg/grass.jpg',
		description = 'Lets save the flower!\nTap on the fly to kill it.'
	},
	{
		speed = 2, maxBugsCount = 4, flowerHealth = 100, level_duration = 30,
		bugs = { 
			{bugName = 'snail', bugWeight = 80 },
			{bugName = 'fly', bugWeight = 20 },
		},
		background = 'images/bg/ground.jpg',
		description = 'Hungry snail!\nTap 3 times to kill it.'
	},
	{
		speed = 2, maxBugsCount = 4, flowerHealth = 100, level_duration = 60,
		bugs = { 
			{bugName = 'ant', bugWeight = 60 },
			{bugName = 'fly', bugWeight = 30 },
			{bugName = 'snail', bugWeight = 10 },
		},
		background = 'images/bg/grass.jpg',
		description = 'Ants are exploring!\nStop them!'
	},
	{
		speed = 4, maxBugsCount = 6, flowerHealth = 100, level_duration = 60,
		bugs = { 
			{bugName = 'ant', bugWeight = 100 },
		},
		background = 'images/bg/ground.jpg',
		description = 'Ants are here again!\nAnd lot of ants!'
	},
	{
		speed = 3, maxBugsCount = 4, flowerHealth = 100, level_duration = 60,
		bugs = { 
			{bugName = 'ant', bugWeight = 30 },
			{bugName = 'fly', bugWeight = 30 },
			{bugName = 'snail', bugWeight = 10 },
			{bugName = 'chafer', bugWeight = 30 },
		},
		background = 'images/bg/wood.jpg',
		description = 'New bug is here!\nTap on chafer 2 times.'
	},
	{
		speed = 3, maxBugsCount = 4, flowerHealth = 100, level_duration = 60,
		bugs = { 
			{bugName = 'snail', bugWeight = 100 },
		},
		background = 'images/bg/grass.jpg',
		description = 'Snail attack!\nHelp!'
	},
	{
		speed = 3, maxBugsCount = 6, flowerHealth = 100, level_duration = 60,
		bugs = { 
			{bugName = 'fly', bugWeight = 20 },
			{bugName = 'chafer', bugWeight = 20 },
			{bugName = 'lady', bugWeight = 60 },
		},
		background = 'images/bg/ground.jpg',
		description = "Lady bug looks cute!\nBut it's hungry as well."
	},
	{
		speed = 3, maxBugsCount = 6, flowerHealth = 100, level_duration = 60,
		bugs = { 
			{bugName = 'earwig', bugWeight = 100 },
		},
		background = 'images/bg/wood.jpg',
		description = 'Earwig!\nBrrrrr...'
	},
	{
		speed = 4, maxBugsCount = 6, flowerHealth = 100, level_duration = 60,
		bugs = { 
			{bugName = 'earwig', bugWeight = 50 },
			{bugName = 'ant', bugWeight = 50 },
		},
		background = 'images/bg/grass.jpg',
		description = 'Ground attack!\nProtect me please!'
	},
	{
		speed = 3, maxBugsCount = 6, flowerHealth = 100, level_duration = 60,
		bugs = { 
			{bugName = 'spider', bugWeight = 50 },
			{bugName = 'ant', bugWeight = 50 },
		},
		background = 'images/bg/ground.jpg',
		description = "Do you like spiders?\nI don't!"
	},
	{
		speed = 3, maxBugsCount = 6, flowerHealth = 100, level_duration = 60,
		bugs = { 
			{bugName = 'spider', bugWeight = 70 },
			{bugName = 'fly', bugWeight = 30 },
		},
		background = 'images/bg/wood.jpg',
		description = "Spiders again!\nAnd in cooperation with fly!"
	},
	{
		speed = 3, maxBugsCount = 6, flowerHealth = 100, level_duration = 60,
		bugs = { 
			{bugName = 'grasshopper', bugWeight = 30 },
			{bugName = 'lady', bugWeight = 70 },
		},
		background = 'images/bg/grass.jpg',
		description = "Hmmm... grasshopper!\nTap on it 2 times for sure!"
	},
	{
		speed = 4, maxBugsCount = 6, flowerHealth = 100, level_duration = 60,
		bugs = { 
			{bugName = 'grasshopper', bugWeight = 100 },
		},
		background = 'images/bg/ground.jpg',
		description = "Grasshopper massive attack!\nLike from the Bible!"
	},
	{
		speed = 3, maxBugsCount = 6, flowerHealth = 80, level_duration = 60,
		bugs = { 
			{bugName = 'snail', bugWeight = 50 },
			{bugName = 'lady', bugWeight = 50 },
		},
		background = 'images/bg/wood.jpg',
		description = "Flower health is't well!\nIt need protection!"
	},
	{
		speed = 2, maxBugsCount = 4, flowerHealth = 100, level_duration = 60,
		bugs = { 
			{bugName = 'worm', bugWeight = 100 },
		},
		background = 'images/bg/grass.jpg',
		description = "Bad dream!\nArmy of the worms!"
	},
	{
		speed = 3, maxBugsCount = 4, flowerHealth = 100, level_duration = 60,
		bugs = { 
			{bugName = 'snail', bugWeight = 30 },
			{bugName = 'worm', bugWeight = 70 },
		},
		background = 'images/bg/ground.jpg',
		description = "Slow bugs!\nHungry bugs!"
	},
	{
		speed = 4, maxBugsCount = 6, flowerHealth = 100, level_duration = 60,
		bugs = { 
			{bugName = 'aphid', bugWeight = 50 },
			{bugName = 'lady', bugWeight = 50 },
		},
		background = 'images/bg/wood.jpg',
		description = "New bug!\nAphid is very destructive!"
	},
	{
		speed = 4, maxBugsCount = 6, flowerHealth = 100, level_duration = 60,
		bugs = { 
			{bugName = 'aphid', bugWeight = 25 },
			{bugName = 'lady', bugWeight = 25 },
			{bugName = 'spider', bugWeight = 25 },
			{bugName = 'ant', bugWeight = 25 },
		},
		background = 'images/bg/grass.jpg',
		description = "Mix of bugs!\nSeems like cooperation!"
	},
	{
		speed = 3, maxBugsCount = 5, flowerHealth = 100, level_duration = 90,
		bugs = { 
			{bugName = 'worm', bugWeight = 50 },
			{bugName = 'fly', bugWeight = 50 },
		},
		background = 'images/bg/ground.jpg',
		description = "Slow and fast,\nworm and fly..."
	},
	{
		speed = 2, maxBugsCount = 2, flowerHealth = 100, level_duration = 90,
		bugs = { 
			{bugName = 'mushi', bugWeight = 100 },
		},
		background = 'images/bg/wood.jpg',
		description = "Ou... exotic one!\n10 times tap is needed!"
	},
}

return _L