local math = require('math')
local table = require( 'table' )

local _M = {}

local _shift = 20
local _time = 3200

local function rotate( o )
	angle = math.random(45) + 25
	
	if o.direction == 'L' then
		angle = -angle
		o.direction = 'R'
	else
		o.direction = 'L'
	end

	o.transition = transition.to( o, {
		rotation = o.rotation + angle, onComplete = rotate, time = _time
	} )
end

function _M.init()
	return display.newGroup( )
end

function _M.show(group)
	local _items = {}

	local imageSheetOptions = {
		width = 300 / 2,
		height = 300 / 2,
		numFrames = 16,
		sheetContentWidth = 1200 / 2,
		sheetContentHeight = 1200 / 2
	}

	local imageSheet = graphics.newImageSheet( 'images/flowers/sunflower.png', imageSheetOptions )

	local flowerDefinition = {
		{start = 13, count = 1, direction = 'L'},
		{start = 9, count = 1, direction = 'R'},
		{start = 5, count = 1, direction = 'L'},
	}
	
	for i = 1, #flowerDefinition do
		local item = flowerDefinition[i]
		local o = display.newImage( imageSheet, item.start )
		
		o.x = halfW
		o.y = halfH
		o.width = 300 / 2
		o.height = 300 / 2
		o.direction = item.direction

		group:insert( o )
		table.insert(_items, o)
	end

	_M.face = display.newSprite( imageSheet, {
		{name = 'happy', start = 1, count = 1},
		{name = 'ok', start = 2, count = 1},
		{name = 'bad', start = 3, count = 1},
		{name = 'dead', start = 4, count = 1},
	} )
	
	_M.face.x = halfW
	_M.face.y = halfH
	_M.face.width = 300 / 2
	_M.face.height = 300 / 2
	_M.face.direction = ''

	group:insert( _M.face )
	table.insert(_items, _M.face)

	group.items = _items

	-- local o = display.newImage( imageSheet, 1 )
	-- o.x = halfW
	-- o.y = halfH
	-- o.width = 299 / 2
	-- o.height = 299 / 2
	-- o.direction = 'L'

	-- group:insert( o )

	group.update_face = _M.update_face

	return group
end

function _M.start(group)
	local _items = group.items
	for i = 1, #_items do
		if _items[i].direction ~= '' then
			rotate(_items[i])
		end
	end
end

function _M.remove(group)
	local _items = group.items
	for i = 1, #_items do
		transition.cancel(_items[i].transtition)
		_items[i]:removeSelf()
	end
end

function _M.pause(group)
	local _items = group.items
	for i = 1, #_items do
		transition.pause(_items[i].transtition)
	end
end

function _M.resume(group)
	local _items = group.items
	for i = 1, #_items do
		transition.resume(_items[i].transtition)
	end
end

function _M.update_face( health )
	utils.debug('update face', health)

	if health >= 70 then
		_M.face:setSequence('happy')
	elseif health >= 40 then
		_M.face:setSequence('ok')
	elseif health > 0 then
		_M.face:setSequence('bad')
	else
		_M.face:setSequence('dead')
	end

	_M.face:play()
end

return _M