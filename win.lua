local composer = require( "composer" )
local scene = composer.newScene()

local widget = require( "widget" )
local timer = require( "timer" )
local transition = require( "transition" )
local native = require( "native" )
local json = require( "json" )

local bug = require( 'bug' )
local common = require( "common" )
local levels = require( "levels" )

local next_level

local current_level
local current_score

local next_level_group
local bg_group
local bg

local bug_timer
local bug_on_menu

function scene:create( event )
	utils.debug('win:scene:create')

	-- initialize the scene
	local sceneGroup = self.view

	bg_group = display.newGroup( )
	sceneGroup:insert( bg_group )

	local flower_defence = display.newImageRect( sceneGroup, 'images/flower-defence.png', 220, 69 )
	flower_defence.x = halfW
	flower_defence.y = 60

	local text = display.newEmbossedText( {
		text = 'Perfect!',
		font = 'GoodDogPlain',
		fontSize = 54,
		x = halfW,
		y = halfH - 55
	} )
	text:setEmbossColor( common.EMBOSS_COLOR )
	sceneGroup:insert( text )

	next_level = display.newEmbossedText( {
		text = '',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW,
		y = halfH - 10
	} )
	next_level:setEmbossColor( common.EMBOSS_COLOR )
	sceneGroup:insert( next_level )

	-- next level button
	next_level_group = display.newGroup( )
	sceneGroup:insert( next_level_group )
	next_level_group.alpha = 0 -- hide this button

	local y = screenH - 170
	local next_level_button = display.newImageRect( next_level_group, 'images/play.png', 54, 54 )
	next_level_button.x = halfW - 40
	next_level_button.y = y

	local next_level_label = display.newEmbossedText( {
		text = 'Next level',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW - 5,
		y = y
	} )
	next_level_label.anchorX = 0
	next_level_label:setEmbossColor( common.EMBOSS_COLOR )
	next_level_group:insert( next_level_label )

	utils.handlerAdd(next_level_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( next_level_group )
		elseif event.phase == 'ended' and common.button_up( next_level_group ) then
			composer.gotoScene( 'game', {
				params = {
					level = current_level + 1,
					score = current_score
				}
			} )
		end

		return true
	end )

	-- replay
	local replay_group = display.newGroup( )
	sceneGroup:insert( replay_group )

	local y = screenH - 110
	local replay_button = display.newImageRect( replay_group, 'images/replay.png', 54, 54 )
	replay_button.x = halfW - 40
	replay_button.y = y

	local replay_label = display.newEmbossedText( {
		text = 'Replay',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW - 5,
		y = y
	} )
	replay_label.anchorX = 0
	replay_label:setEmbossColor( common.EMBOSS_COLOR )
	replay_group:insert( replay_label )

	utils.handlerAdd(replay_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( replay_group )
		elseif event.phase == 'ended' and common.button_up( replay_group ) then
			composer.gotoScene( 'game', {
				params = {
					level = current_level,
					score = current_score
				}
			} )
		end

		return true
	end )

	-- main menu
	local menu_group = display.newGroup( )
	sceneGroup:insert( menu_group )

	local y = screenH - 50
	local menu_button = display.newImageRect( menu_group, 'images/menu.png', 54, 54 )
	menu_button.x = halfW - 40
	menu_button.y = y

	local menu_label = display.newEmbossedText( {
		text = 'Main menu',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW - 5,
		y = y
	} )
	menu_label.anchorX = 0
	menu_label:setEmbossColor( common.EMBOSS_COLOR )
	menu_group:insert( menu_label )

	utils.handlerAdd(menu_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( menu_group )
		elseif event.phase == 'ended' and common.button_up( menu_group ) then
			composer.gotoScene( 'menu' )
		end

		return true
	end )
end

function scene:show( event )
	utils.debug('win:scene:show', event.phase)

	if event.phase == 'will' then
		current_level = event.params.currentLevel
		current_score = event.params.score

		if event.params.nextLevel > #levels.levels then
			-- all levels done
			next_level.text = 'Your flower is safe now!'

			next_level_group.alpha = 0
		else
			-- play next level
			next_level.text = 'Prepare for level ' .. event.params.nextLevel .. '!'

			next_level_group.alpha = 1
		end

		-- add background
		bg = common.background( levels.levels[event.params.currentLevel].background )
		bg_group:insert( bg )

		bug_timer = timer.performWithDelay( 2000, function()
			-- 50% skip
			if math.random( 2 ) == 1 then return end

			-- create bug
			bug_on_menu = bug.bug_on_menu()
			bug_on_menu:setSequence('walk')
			bug_on_menu:play( )

			bug_on_menu.move()
		end, -1)
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	if event.phase == 'will' then

	elseif event.phase == 'did' then
		timer.cancel( bug_timer )
		display.remove( bug_on_menu )
	end
end

function scene:destroy( event )
	local sceneGroup = self.view
	
end

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene