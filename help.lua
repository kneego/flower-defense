local composer = require( "composer" )
local scene = composer.newScene()

local physics = require( "physics" )
local widget = require( "widget" )

local sunflower = require('flower.sunflower')
local common = require( "common" )
local bug = require( "bug" )

local level_duration = 30

local flowerObject
local levelNumber
local levelDescription
local duration
local scoreText
local coin
local pauseBtn


function scene:create( event )
	-- initialize the scene
	local sceneGroup = self.view

	local bg = common.background()
	sceneGroup:insert( bg )

	utils.handlerAdd( bg, 'touch', function( self, event )
		if event.phase == 'began' then
			composer.gotoScene( 'level_selection' )
		end
	end )

	flowerObject = sunflower.init()
	sceneGroup:insert( flowerObject )

	levelNumber = common.levelNumber( sceneGroup )
	levelDescription = common.levelDescription( sceneGroup )

	duration, scoreText, coin, pauseBtn = common.buildHID( sceneGroup )
	health_indicator = common.health_indicator( sceneGroup )

	local your_score = display.newEmbossedText( {
		text = 'Your\nscore',
		font = 'GoodDogPlain',
		fontSize = 24,
		x = screenW - 30,
		y = 100,
		width = 100,
		height = 50,
		align = 'right',
	} )
	your_score:setEmbossColor( common.EMBOSS_COLOR )
	your_score.anchorX = 1
	sceneGroup:insert( your_score )

	local timer_text = display.newEmbossedText( {
		text = 'Timer',
		font = 'GoodDogPlain',
		fontSize = 24,
		x = 30,
		y = 100
	} )
	timer_text:setEmbossColor( common.EMBOSS_COLOR )
	timer_text.anchorX = 0
	sceneGroup:insert( timer_text )

	local pause_text = display.newEmbossedText( {
		text = 'Pause\ngame',
		font = 'GoodDogPlain',
		fontSize = 24,
		x = halfW,
		y = 110,
		width = 100,
		height = 50,
		align = 'center',
	} )
	pause_text:setEmbossColor( common.EMBOSS_COLOR )
	sceneGroup:insert( pause_text )

	local flower_text = display.newEmbossedText( {
		text = 'Protect\nflower',
		font = 'GoodDogPlain',
		fontSize = 24,
		x = 30,
		y = 170,
		width = 100,
		height = 50,
	} )
	flower_text:setEmbossColor( common.EMBOSS_COLOR )
	flower_text.anchorX = 0
	sceneGroup:insert( flower_text )

	local health_text = display.newEmbossedText( {
		text = 'Flower\nhealth',
		font = 'GoodDogPlain',
		fontSize = 24,
		x = 40,
		y = screenH - 80,
		width = 100,
		height = 50,
	} )
	health_text:setEmbossColor( common.EMBOSS_COLOR )
	health_text.anchorX = 0
	sceneGroup:insert( health_text )

	local fly = bug.bug_on_help()
	sceneGroup:insert( fly )

	local fly_text = display.newEmbossedText( {
		text = 'Tap\nto kill\nbugs',
		font = 'GoodDogPlain',
		fontSize = 24,
		x = screenW - 10,
		y = screenH - 180,
		width = 100,
		height = 100,
		align = 'right',
	} )
	fly_text:setEmbossColor( common.EMBOSS_COLOR )
	fly_text.anchorX = 1
	sceneGroup:insert( fly_text )
end

function scene:show( event )
	utils.debug(event.phase)
	if event.phase == 'will' then
		sunflower.show(flowerObject)

		duration.text = 30
		scoreText.text = 123
		health_indicator.update( 100, false )

		-- update position
		scoreText.x = screenW - 10 - string.len(scoreText.text) * 10
		coin.x = screenW - 10 - string.len(scoreText.text) * 10 - 32 - 5

		pauseBtn.alpha = 1

		settings.show_help = false
		utils.saveSettings( settings )
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- pause the scene (stop timers, stop animation, unload sounds, etc.)

	elseif phase == "did" then
		-- Called when the scene is now off screen
		
	end	
	
end

function scene:destroy( event )
	local sceneGroup = self.view

end

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene