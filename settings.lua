local io = require( "io" )
local json = require( "json" )
local system = require( "system" )

function loadSettings()
	local path = system.pathForFile( "settings.json", system.DocumentsDirectory )
	local file = io.open( path, "r" )

	if file then
		-- load state
		local savedDate = file:read( "*a" )
		
		io.close( file )
		
		file = nil

		return json.decode( savedDate )
	else
		-- create empty data and save it
		local data = {
			score = 0,
			lives = 3,
			selectedCategory = 2,
			sounds = true
		}

		saveSettings(data)

		return data
	end
end

function saveSettings(dataToSave)
	local jsonData = json.encode( dataToSave )

	local path = system.pathForFile( "settings.json", system.DocumentsDirectory )
	local file = io.open( path, "w" )

	file:write( jsonData )

	io.close( file )

	file = nil
end

return {
	loadSettings = loadSettings,
	saveSettings = saveSettings
}