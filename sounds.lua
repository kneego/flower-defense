local _M = {}

_M.sound = {
	-- music = audio.loadSound( "sounds/music.mp3" ),

	button_click = audio.loadSound( "sounds/button-click.wav" ),
	success = audio.loadSound( "sounds/success.mp3" ),
	fail = audio.loadSound( "sounds/fail.mp3" ),

	fly = audio.loadSound( "sounds/fly.mp3" ),

	splatch1 = audio.loadSound( "sounds/splatch-1.mp3" ),
	splatch2 = audio.loadSound( "sounds/splatch-2.mp3" ),

	music = audio.loadSound( "sounds/music.mp3" ),
}

_M.channel = {
	COMMON_CHANNEL = 1,
	SFX_CHANNEL = 2,
	MUSIC_CHANNEL = 3,
	FLY_CHANNEL = 4,
	BUG_CHANNEL = 5,
}

function _M.play( sound, channel, loops)
	-- utils.debug('game:play_sound', loops)
	if settings.sounds then
		if channel == nil then channel = _M.channel.COMMON_CHANNEL end
		if loops == nil then loops = 0 end

		-- utils.debug('game:play_sound', channel)

		audio.stop( channel )

		audio.play( sound, {channel = channel, loops = loops} )
	end
end

function _M.stop( channel )
	if settings.sounds then
		audio.stop( channel )
	end
end

function _M.vibrate()
	if settings.vibrations then
		system.vibrate()
	end
end

return _M