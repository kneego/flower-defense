local _M = {}

_M.shakeCount = 0
_M.xShake = 2
_M.yShake = 2
_M.shakePeriod = 2

_M.original_x = display.currentStage.x
_M.original_y = display.currentStage.y

_M.shaking = false

function _M.shake()
	if(_M.shakeCount % _M.shakePeriod == 0 ) then
		display.currentStage.x = display.currentStage.x0 + math.random( -_M.xShake, _M.xShake )
		display.currentStage.y = display.currentStage.y0 + math.random( -_M.yShake, _M.yShake )
	end

	_M.shakeCount = _M.shakeCount + 1
end

function _M.start()
	if _M.shaking then return end

	_M.shaking = true

	display.currentStage.x0 = display.currentStage.x
	display.currentStage.y0 = display.currentStage.y

	_M.shakeCount = 0

	Runtime:addEventListener( "enterFrame", _M.shake )
end

function _M.stop()
	Runtime:removeEventListener( "enterFrame", _M.shake )

	timer.performWithDelay( 1, function()			
		display.currentStage.x = display.currentStage.x0 
		display.currentStage.y = display.currentStage.y0
	end )		

	_M.shaking = false
end

function _M.reset()
	display.currentStage.x = _M.original_x
	display.currentStage.y = _M.original_y
end

return _M