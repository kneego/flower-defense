--
-- Reactor module for Corona SDK
-- http://www.reactor.am/docs/
--
-- Pavel Koci (@pavelkoci), KneeGo (@KneeGoApps)
--
-- https://bitbucket.org/pavelkoci/corona-reactor-module/
--

local json = require( "json" )
local system = require( "system" )
local network = require( "network" )

local version = 1.0 -- lib version

local function networkListener(event)
	if event.isError then
		print ("Reactor request failed.")
	end
end

local reactor = {}

function reactor:init(application_id)
	--[[
	application_id - required parameter. application_id will be send with each request.
	]]

	self.application_id = application_id

	self.appName = system.getInfo( "appName" )
	self.appVersion = system.getInfo( "appVersionString" )
	self.deviceID = system.getInfo( "deviceID" )

	local headers = {}

	headers["User-Agent"] = "Reactor/" .. version .. " (Corona; U; " ..
		system.getInfo( "platformName" ) .. " " .. system.getInfo( "platformVersion" ) .. "; " ..
		system.getPreference( "locale", "identifier" ) .. "; " ..
		system.getInfo( "platformVersion" ) .. ")"
	
	self.params = {}
	self.params.headers = headers
end

function reactor:event(user_id, event, data)
	--[[
	user_id - required parameter. user_id will be send with each request.
	event - name of the event. (login, logout, ...)
	data - contains additional event data or can be nil or empty table

	reactor:event('user123', 'start_game', {
		level_i = 1
	})
	]]

	if data == nil then data = {} end

	data.type = event
	data.user_id = user_id
	data.application_id = self.application_id

	self:sendData( {
		type = "event",
		data = data
	} )
end

function reactor:collect(user_id, data)
	--[[
	user_id - required parameter. user_id will be send with each request.
	data - table with data to collect

	reactor:collect('user123', {
		first_name_s = 'John',
		last_name_s = 'Stone',
		age_i = 33
	})
	]]

	data.user_id = user_id
	data.application_id = self.application_id

	self:sendData( {
		type = 'collect',
		data = data
	} )
end

function reactor:bulk(user_id, data)
	--[[
	user_id - required parameter. user_id will be send with each request.
	data - table with bulk collect or event data.
	
	reactor:bulk('user123', {
		{type = 'event', data = {type = 'start_game', level_i = 1}},
		{type = 'collect', data = {first_name_s = 'John', last_name_s = 'Stone'}},
		{type = 'event', data = {type = 'stop_game', score_i = 123}},
	})
	]]

	if type(data) ~= 'table' then print('reactor:bulk table required') return end

	_data = {}
	for i=1, #data do
		_item = data[i]
		if type(_item) == 'table' then
			_item.data.v_f = version
			_item.data.did_s = self.deviceID
			_item.data.an_s = self.appName
			_item.data.av_s = self.appVersion

			table.insert( _data, _item )
		else
			print('reactor:bulk every item must be a table')
			break
		end
	end

	params = self.params
	params.body = json.encode( _data )

	network.request(
		"https://api/collector/_bulk/" .. self.application_id .. "/" .. user_id .. "/",
		"POST",
		networkListener,
		params
	)
end

function reactor:sendData( data )
	data.data.v_f = version
	data.data.did_s = self.deviceID
	data.data.an_s = self.appName
	data.data.av_s = self.appVersion

	params = self.params
	params.body = json.encode( data )

	network.request(
		"https://api.reactor.am/collector/",
		"POST",
		networkListener,
		params
	)
end

return reactor