local composer = require( "composer" )
local scene = composer.newScene()

local widget = require( "widget" )
local timer = require( "timer" )
local transition = require( "transition" )
local native = require( "native" )
local json = require( "json" )

local bug = require( 'bug' )
local common = require( "common" )
local levels = require( "levels" )

local replay
local menu
local board
local bg_group
local bg

local bug_timer
local bug_on_menu

local level_number
local score

local score_text

function scene:create( event )
	utils.debug('game_over:scene:create')

	-- initialize the scene
	local sceneGroup = self.view

	bg_group = display.newGroup( )
	sceneGroup:insert( bg_group )

	local flower_defence = display.newImageRect( sceneGroup, 'images/flower-defence.png', 220, 69 )
	flower_defence.x = halfW
	flower_defence.y = 60

	local text = display.newEmbossedText( {
		text = 'Game Over',
		font = 'GoodDogPlain',
		fontSize = 54,
		x = halfW,
		y = halfH - 55
	} )
	text:setEmbossColor( common.EMBOSS_COLOR )
	sceneGroup:insert( text )

	score_text = display.newEmbossedText( {
		text = '',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW,
		y = halfH - 10
	} )
	score_text:setEmbossColor( common.EMBOSS_COLOR )
	sceneGroup:insert( score_text )

	-- replay
	local replay_group = display.newGroup( )
	sceneGroup:insert( replay_group )

	local y = screenH - 170
	local replay_button = display.newImageRect( replay_group, 'images/replay.png', 54, 54 )
	replay_button.x = halfW - 40
	replay_button.y = y

	local replay_label = display.newEmbossedText( {
		text = 'Replay',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW - 5,
		y = y
	} )
	replay_label.anchorX = 0
	replay_label:setEmbossColor( common.EMBOSS_COLOR )
	replay_group:insert( replay_label )

	utils.handlerAdd(replay_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( replay_group )
		elseif event.phase == 'ended' and common.button_up( replay_group ) then
			composer.gotoScene( 'game', {
				params = {
					level = level_number
				}
			} )
		end

		return true
	end )

	-- leadeboard
	local leaderboard_group = display.newGroup( )
	sceneGroup:insert( leaderboard_group )

	local y = screenH - 110
	local leaderboard_button = display.newImageRect( leaderboard_group, 'images/leaderboard.png', 54, 54 )
	leaderboard_button.x = halfW - 40
	leaderboard_button.y = y

	local leaderboard_label = display.newEmbossedText( {
		text = R.strings('leaderboard'),
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW - 5,
		y = y
	} )
	leaderboard_label.anchorX = 0
	leaderboard_label:setEmbossColor( common.EMBOSS_COLOR )
	leaderboard_group:insert( leaderboard_label )

	utils.handlerAdd( leaderboard_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( leaderboard_group )
		elseif event.phase == 'ended' and common.button_up( leaderboard_group ) then
			game_network.show_leaderboards()
		end

		return true
	end )

	-- main menu
	local menu_group = display.newGroup( )
	sceneGroup:insert( menu_group )

	local y = screenH - 50
	local menu_button = display.newImageRect( menu_group, 'images/menu.png', 54, 54 )
	menu_button.x = halfW - 40
	menu_button.y = y

	local menu_label = display.newEmbossedText( {
		text = 'Main menu',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW - 5,
		y = y
	} )
	menu_label.anchorX = 0
	menu_label:setEmbossColor( common.EMBOSS_COLOR )
	menu_group:insert( menu_label )

	utils.handlerAdd(menu_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( menu_group )
		elseif event.phase == 'ended' and common.button_up( menu_group ) then
			composer.gotoScene( 'menu' )
		end

		return true
	end )
end

function scene:show( event )
	utils.debug('game_over:scene:show')

	if event.phase == 'will' then
		level_number = event.params.currentLevel

		-- add background
		bg = common.background( levels.levels[event.params.currentLevel].background )
		bg_group:insert( bg )

		score_text.text = R.strings('your_score') .. event.params.score

		bug_timer = timer.performWithDelay( 2000, function()
			-- 50% skip
			if math.random( 2 ) == 1 then return end

			-- create bug
			bug_on_menu = bug.bug_on_menu()
			bug_on_menu:setSequence('walk')
			bug_on_menu:play( )

			bug_on_menu.move()
		end, -1)
	end
end

function scene:hide( event )
	local sceneGroup = self.view

	if event.phase == "will" then
		-- pause the scene (stop timers, stop animation, unload sounds, etc.)

	elseif event.phase == "did" then
		timer.cancel( bug_timer )
		display.remove( bug_on_menu )
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
end

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene