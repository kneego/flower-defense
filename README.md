# Flower Defence

Full source code and assets of Flower Defence game for [Android](https://play.google.com/store/apps/details?id=org.kneego.flowerdefence) and [iOS](https://itunes.apple.com/us/app/flower-defence/id1044627599?ls=1&mt=8).

The game uses the [Corona SDK](https://coronalabs.com) game engine.
