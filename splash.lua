local _M = {}

function _M.splash( image_path )
	local imageSheetOptions = {
		width = 200,
		height = 200,
		numFrames = 4,
		sheetContentWidth = 800,
		sheetContentHeight = 200,
	}

	local imageSheet = graphics.newImageSheet( image_path, imageSheetOptions )

	local o = display.newSprite( imageSheet, {
		{name = 'splash', start = 1, count = 4, loopCount = 1},
	} )

	o.rotation = math.random( 360 )

	o:play( 'splash' )

	transition.to( o, {
		alpha = 0,
		time = 5000
	} )

	return o
end

function _M.green()
	return _M.splash( 'images/splash/green.png' )
end

function _M.pink()
	return _M.splash( 'images/splash/pink.png' )
end

function _M.yellow()
	return _M.splash( 'images/splash/yellow.png' )
end

function _M.green_red()
	return _M.splash( 'images/splash/green-red.png' )
end

function _M.green_yellow()
	return _M.splash( 'images/splash/green-yellow.png' )
end

return _M