local composer = require( "composer" )
local scene = composer.newScene()

local physics = require( "physics" )
local widget = require( "widget" )

local common = require( "common" )

local bg
local audioSwitch

function scene:create( event )
	-- initialize the scene
	local sceneGroup = self.view

	bg = common.background()
	sceneGroup:insert( bg )

	local flower_defence = display.newImageRect( sceneGroup, 'images/flower-defence.png', 220, 69 )
	flower_defence.x = halfW
	flower_defence.y = 60

	-- audio button
	audioSwitch = common.audioSwitchIcon()
	sceneGroup:insert( audioSwitch )

	utils.handlerAdd( audioSwitch, 'touch', function( self, event )
		if event.phase == 'began' then
			common.button_down( audioSwitch )
		elseif event.phase == 'ended' and common.button_up( audioSwitch ) then
			-- change sounds state
			if settings.sounds then
				settings.sounds = false
				audioSwitch.setSequence( 'off' )
			else
				settings.sounds = true
				audioSwitch.setSequence( 'on' )
			end

			utils.saveSettings( settings )
			audioSwitch.play( )
		end

		return true
	end )

	-- music button
	musicSwitch = common.musicSwitchIcon()
	sceneGroup:insert( musicSwitch )

	utils.handlerAdd( musicSwitch, 'touch', function( self, event )
		if event.phase == 'began' then
			common.button_down( musicSwitch )
		elseif event.phase == 'ended' and common.button_up( musicSwitch ) then
			-- change sounds state
			if settings.music then
				settings.music = false
				musicSwitch.setSequence( 'off' )
			else
				settings.music = true
				musicSwitch.setSequence( 'on' )
			end

			utils.saveSettings( settings )
			musicSwitch.play( )
		end

		return true
	end )

	-- main menu
	local menu_group = display.newGroup( )
	sceneGroup:insert( menu_group )

	local y = screenH - 50
	local menu_button = display.newImageRect( menu_group, 'images/menu.png', 54, 54 )
	menu_button.x = halfW - 40
	menu_button.y = y

	local menu_label = display.newEmbossedText( {
		text = 'Main menu',
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW - 5,
		y = y
	} )
	menu_label.anchorX = 0
	menu_label:setEmbossColor( common.EMBOSS_COLOR )
	menu_group:insert( menu_label )

	utils.handlerAdd(menu_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( menu_group )
		elseif event.phase == 'ended' and common.button_up( menu_group ) then
			composer.gotoScene( 'menu' )
		end

		return true
	end )
end

function scene:show( event )
	utils.debug(event.phase)
	if event.phase == 'will' then
		-- init audio swich
		if settings.sounds then
			audioSwitch.setSequence( 'on' )
		else
			audioSwitch.setSequence( 'off' )
		end

		audioSwitch.play( )

		-- init music swich
		if settings.music then
			musicSwitch.setSequence( 'on' )
		else
			musicSwitch.setSequence( 'off' )
		end

		musicSwitch.play( )
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- pause the scene (stop timers, stop animation, unload sounds, etc.)

	elseif phase == "did" then
		-- Called when the scene is now off screen
		
	end	
	
end

function scene:destroy( event )
	local sceneGroup = self.view

end

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene