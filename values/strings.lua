local _M = {}

_M.default = {
	-- menu
	play_label = 'Play',
	back_label = 'Back',
	menu_label = 'Menu',

	facebook_url = 'https://m.facebook.com/Kneego-123462307855125/',
	twitter_url = 'https://m.twitter.com/KneeGoApps',

	your_score = 'Your score: ',
	leaderboard = 'Leaderboard',
}

_M.sk = {
	-- menu
	play_label = 'Hraj',
	back_label = 'Späť',
	menu_label = 'Menu',
}

_M.cz = {
    -- menu
    play_label = 'Hraj',
    back_label = 'Zpět',
    menu_label = 'Menu',
}

return _M