local _M = {
	default = {},
	sk = {},
	cs = {},
}

_M.default.admob_id = {
	['iPhone OS'] = 'ca-app-pub-1210274935500874/8234467541',
	['Android'] = 'ca-app-pub-1210274935500874/6757734341'
}

_M.default.board_id = {
	['iPhone OS'] = {
		top = 'top_players',
	},
	['Android'] = {
		top = 'CgkInZDOkM0UEAIQAA',
	}
}

return _M