local splash = require( 'splash' )

local _L = {}

local splash_map = {
	green = splash.green,
	pink = splash.pink,
	yellow = splash.yellow,
	green_red = splash.green_red,
	green_yellow = splash.green_yellow,
}

local function getBugPosition(target_x, target_y, size, source_x, source_y)
	local shift_x = math.random(40) - 20
	local shift_y = math.random(40) - 20

	if size == nil then size = 20 end

	if target_x == nil then
		target_x = halfW + shift_x
	end

	if target_y == nil then
		target_y = halfH - shift_y
	end

	if source_x ~= nil and source_y ~= nil then
		x = source_x
		y = source_y
	else
		-- calculate starting position
		if math.random( 2 ) == 1 then
			-- x position will be variable
			x = math.random(screenW)

			if math.random( 2 ) == 1 or target_y >= screenH then
				y = -size
			else
				y = screenH + size
			end
		else
			-- y position will be variable
			y = math.random(screenH)

			if math.random( 2 ) == 1 or target_x >= screenW then
				x = -size
			else
				x = screenW + size
			end
		end
	end

	local rotation = math.deg(math.atan2(target_x - x, y - target_y))

	return x, y, shift_x, shift_y, rotation
end

local function add_common_methods( o, target_x, target_y, on_complete, on_start )
	o.move = function ()
		if target_x == nil then
			target_x = halfW + o.shift_x
		end

		if target_y == nil then
			target_y = halfH + o.shift_y
		end

		if on_start ~= nil then
			on_start()
		end

		if o.sound ~= nil then
			-- play sound
			sounds.play( o.sound, sounds.channel.BUG_CHANNEL )
		end

		o.transition = transition.moveTo( o, {
			x = target_x,
			y = target_y,
			time = o.speed + (math.random(o.speed * 0.1) - o.speed * 0.2),
			onComplete = on_complete,
		} )
	end

	o.stopMoving = function()
		transition.cancel( o.transition )
	end

	o.pause_move = function ( ... )
		transition.pause( o.transition )

		timer.performWithDelay( 60, function ( ... )
			transition.resume( o.transition )
		end )
	end

	o.splash = function()
		local splash_object = splash_map[o.splash_type]()
		splash_object.x = o.x
		splash_object.y = o.y
	
		return splash_object
	end

	return o
end

function _L.bug_on_help( ... )
	local imageSheetOptions = {
		width = 75 * 0.8,
		height = 85 * 0.8,
		numFrames = 9,
		sheetContentWidth = 75 * 0.8 * 9,
		sheetContentHeight = 85 * 0.8,
	}

	local imageSheet = graphics.newImageSheet( 'images/bugs/fly.png', imageSheetOptions )

	local o = display.newSprite( imageSheet, {
		{name = 'eat', start = 1, count = 1},
		{name = 'walk', start = 1, count = 8},
		{name = 'dead', start = 9, count = 1}
	} )

	x, y, shift_x, shift_y, rotation = getBugPosition(halfW, halfH, nil, screenW - 100, screenH - 100)

	o.x = x
	o.y = y
	o.rotation = rotation
	o.speed = (math.abs(halfW - x) + math.abs(halfH - y)) * 2.5

	return o
end

function _L.bug_on_menu( ... )
	local imageSheetOptions = {
		width = 75 * 0.8,
		height = 85 * 0.8,
		numFrames = 9,
		sheetContentWidth = 75 * 0.8 * 9,
		sheetContentHeight = 85 * 0.8,
	}

	local imageSheet = graphics.newImageSheet( 'images/bugs/fly.png', imageSheetOptions )

	local o = display.newSprite( imageSheet, {
		{name = 'eat', start = 1, count = 1},
		{name = 'walk', start = 1, count = 8},
		{name = 'dead', start = 9, count = 1}
	} )

	local target_x, target_y, _, x, y, shift_x, shift_y, rotation = 0, 0, 0, 0, 0, 0, 0, 0

	-- sorce position
	while (math.abs(target_x - x) + math.abs(target_y - y)) < 200 do
		target_x, target_y, _, _, _ = getBugPosition()
		x, y, shift_x, shift_y, rotation = getBugPosition(target_x, target_y)
	end

	o.shift_x = shift_x
	o.shift_y = shift_y
	o = add_common_methods( o, target_x, target_y, function( target )
		display.remove( target )

		local current_volume = audio.getVolume( { channel = sounds.channel.FLY_CHANNEL } )
		audio.fadeOut({ channel = sounds.channel.FLY_CHANNEL, time = 200 })

		timer.performWithDelay( 200, function ( ... )
			sounds.stop( sounds.channel.FLY_CHANNEL )

			audio.setVolume( current_volume, {channel = sounds.channel.FLY_CHANNEL} )
		end )
	end, function ()
		sounds.play( sounds.sound.fly, sounds.channel.FLY_CHANNEL )
	end )

	o.x = x
	o.y = y
	o.rotation = rotation
	o.speed = (math.abs(target_x - x) + math.abs(target_y - y)) * 2.5

	return o
end

function _L.fly(x, y)
	local imageSheetOptions = {
		width = 75 * 0.8,
		height = 85 * 0.8,
		numFrames = 9,
		sheetContentWidth = 75 * 0.8 * 9,
		sheetContentHeight = 85 * 0.8,
	}

	local imageSheet = graphics.newImageSheet( 'images/bugs/fly.png', imageSheetOptions )

	local o = display.newSprite( imageSheet, {
		{name = 'eat', start = 1, count = 1},
		{name = 'walk', start = 1, count = 8},
		{name = 'dead', start = 9, count = 1}
	} )

	local x, y, shift_x, shift_y, rotation = getBugPosition()
	o.shift_x = shift_x
	o.shift_y = shift_y
	o = add_common_methods( o )

	o.x = x
	o.y = y
	o.rotation = rotation

	-- game settings
	o.power = 1
	o.life = 1
	o.score = 1
	o.speed = 2000
	o.splash_type = "green"
	o.sound = sounds.sound.fly

	return o
end

function _L.ant(x, y)
	local imageSheetOptions = {
		width = 119 * 0.9 / 2,
		height = 150 * 0.9 / 2,
		numFrames = 8,
		sheetContentWidth = 949 * 0.9 / 2,
		sheetContentHeight = 150 * 0.9 / 2
	}

	local imageSheet = graphics.newImageSheet( 'images/bugs/ant.png', imageSheetOptions )

	local o = display.newSprite( imageSheet, {
		{name = 'eat', start = 1, count = 1},
		{name = 'walk', start = 1, count = 8},
		{name = 'dead', start = 1, count = 1}
	} )

	local x, y, shift_x, shift_y, rotation = getBugPosition()
	o.shift_x = shift_x
	o.shift_y = shift_y
	o = add_common_methods( o )

	o.x = x
	o.y = y
	o.width = 28 * 1.4
	o.height = 36 * 1.4
	o.rotation = rotation

	-- game settings
	o.power = 1
	o.life = 1
	o.score = 2
	o.speed = 5000
	o.splash_type = "yellow"

	return o
end

function _L.snail(x, y)
	local imageSheetOptions = {
		width = 640 / 8 / 2,
		height = 440 / 2,
		numFrames = 8,
		sheetContentWidth = 640 / 2,
		sheetContentHeight = 440 / 2,
	}

	local imageSheet = graphics.newImageSheet( 'images/bugs/snail.png', imageSheetOptions )

	local o = display.newGroup( )

	local bug = display.newSprite( imageSheet, {
		{name = 'eat', start = 1, count = 1},
		{name = 'walk', start = 1, count = 6, time = 500},
		{name = 'dead', start = 8, count = 1}
	} )
	o:insert( bug )

	local x, y, shift_x, shift_y, rotation = getBugPosition(nil, nil, 40)
	o.shift_x = shift_x
	o.shift_y = shift_y
	o = add_common_methods( o )

	o.x = x
	o.y = y
	-- o.width = 50
	-- o.height = 100
	o.rotation = rotation

	-- game settings
	o.power = 3
	o.life = 3
	o.score = 4
	o.speed = 10000
	o.shake = true
	o.splash_type = "pink"
	o.shift_added_y = -16

	-- custom methods
	o.setSequence = function( self, name )
		bug:setSequence( name )
	end

	o.play = function( self )
		bug:play()
	end

	o.pause = function( self )
		bug:pause()
	end

	o.add_to_group = function( object )
		o:insert( object )

		object.y = o.shift_added_y
	end

	return o
end

function _L.chafer(x, y)
	local imageSheetOptions = {
		width = 120 / 2,
		height = 180 / 2,
		numFrames = 8,
		sheetContentWidth = 960 / 2,
		sheetContentHeight = 180 / 2,
	}

	local imageSheet = graphics.newImageSheet( 'images/bugs/cockchafer.png', imageSheetOptions )

	local o = display.newSprite( imageSheet, {
		{name = 'eat', start = 1, count = 1},
		{name = 'walk', start = 1, count = 8},
		{name = 'dead', start = 1, count = 1}
	} )

	local x, y, shift_x, shift_y, rotation = getBugPosition()
	o.shift_x = shift_x
	o.shift_y = shift_y
	o = add_common_methods( o )

	o.x = x
	o.y = y
	o.rotation = rotation

	-- game settings
	o.power = 2
	o.life = 2
	o.score = 4
	o.speed = 6000
	o.splash_type = "yellow"

	return o
end

function _L.lady(x, y)
	local imageSheetOptions = {
		width = 100 / 1.538288,
		height = 95 / 1.538288,
		numFrames = 8,
		sheetContentWidth = 800 / 1.538288,
		sheetContentHeight = 95 / 1.538288,
	}

	local imageSheet = graphics.newImageSheet( 'images/bugs/lady.png', imageSheetOptions )

	local o = display.newSprite( imageSheet, {
		{name = 'eat', start = 1, count = 1},
		{name = 'walk', start = 1, count = 8},
		{name = 'dead', start = 1, count = 1}
	} )

	local x, y, shift_x, shift_y, rotation = getBugPosition()
	o.shift_x = shift_x
	o.shift_y = shift_y
	o = add_common_methods( o )

	o.x = x
	o.y = y
	o.rotation = rotation

	-- game settings
	o.power = 1
	o.life = 1
	o.score = 1
	o.speed = 4500
	o.splash_type = "yellow"

	return o
end

function _L.earwig(x, y)
	local imageSheetOptions = {
		width = 1280 * 0.8 / 8 / 2,
		height = 228 * 0.8 / 2,
		numFrames = 8,
		sheetContentWidth = 1280 * 0.8 / 2,
		sheetContentHeight = 228 * 0.8 / 2,
	}

	local imageSheet = graphics.newImageSheet( 'images/bugs/earwig.png', imageSheetOptions )

	local o = display.newSprite( imageSheet, {
		{name = 'eat', start = 1, count = 1},
		{name = 'walk', start = 1, count = 8},
		{name = 'dead', start = 1, count = 1}
	} )

	local x, y, shift_x, shift_y, rotation = getBugPosition()
	o.shift_x = shift_x
	o.shift_y = shift_y
	o = add_common_methods( o )

	o.x = x
	o.y = y
	o.rotation = rotation

	-- game settings
	o.power = 2
	o.life = 2
	o.score = 2
	o.speed = 5000
	o.splash_type = "green_yellow"

	return o
end

function _L.spider(x, y)
	local imageSheetOptions = {
		width = 85 * 0.7,
		height = 130 * 0.7,
		numFrames = 8,
		sheetContentWidth = 680 * 0.7,
		sheetContentHeight = 130 * 0.7,
	}

	local imageSheet = graphics.newImageSheet( 'images/bugs/spider.png', imageSheetOptions )

	local o = display.newSprite( imageSheet, {
		{name = 'eat', start = 1, count = 1},
		{name = 'walk', start = 1, count = 8},
		{name = 'dead', start = 1, count = 1}
	} )

	local x, y, shift_x, shift_y, rotation = getBugPosition()
	o.shift_x = shift_x
	o.shift_y = shift_y
	o = add_common_methods( o )

	o.x = x
	o.y = y
	o.rotation = rotation

	-- game settings
	o.power = 1
	o.life = 2
	o.score = 3
	o.speed = 6500
	o.splash_type = "pink"

	return o
end

function _L.grasshopper(x, y)
	local imageSheetOptions = {
		width = 125 * 0.7,
		height = 220 * 0.7,
		numFrames = 7,
		sheetContentWidth = 875 * 0.7,
		sheetContentHeight = 220 * 0.7,
	}

	local imageSheet = graphics.newImageSheet( 'images/bugs/grasshop.png', imageSheetOptions )

	local o = display.newSprite( imageSheet, {
		{name = 'eat', start = 1, count = 1},
		{name = 'walk', start = 1, count = 8},
		{name = 'dead', start = 1, count = 1}
	} )

	local x, y, shift_x, shift_y, rotation = getBugPosition()
	o.shift_x = shift_x
	o.shift_y = shift_y
	o = add_common_methods( o )

	o.x = x
	o.y = y
	o.width = 42
	o.height = 50
	o.rotation = rotation

	-- game settings
	o.power = 2
	o.life = 2
	o.score = 3
	o.speed = 4000
	o.splash_type = "green_red"

	return o
end

function _L.worm(x, y)
	local imageSheetOptions = {
		width = 40,
		height = 280 / 2,
		numFrames = 7,
		sheetContentWidth = 560 / 2,
		sheetContentHeight = 280 / 2,
	}

	local imageSheet = graphics.newImageSheet( 'images/bugs/worm.png', imageSheetOptions )

	local o = display.newSprite( imageSheet, {
		{name = 'eat', start = 1, count = 1},
		{name = 'walk', start = 1, count = 6, time = 500},
		{name = 'dead', start = 1, count = 1},
	} )

	local x, y, shift_x, shift_y, rotation = getBugPosition()
	o.shift_x = shift_x
	o.shift_y = shift_y
	o = add_common_methods( o )

	o.x = x
	o.y = y
	o.rotation = rotation

	-- game settings
	o.power = 3
	o.life = 4
	o.score = 3
	o.speed = 9500
	o.shake = true
	o.splash_type = "green_yellow"

	return o
end

function _L.aphid(x, y)
	local imageSheetOptions = {
		width = 65 / 1.2,
		height = 85 / 1.2,
		numFrames = 8,
		sheetContentWidth = 520 / 1.2,
		sheetContentHeight = 85 / 1.2
	}

	local imageSheet = graphics.newImageSheet( 'images/bugs/aphid.png', imageSheetOptions )

	local o = display.newSprite( imageSheet, {
		{name = 'eat', start = 1, count = 1},
		{name = 'walk', start = 1, count = 8},
		{name = 'dead', start = 1, count = 1}
	} )

	local x, y, shift_x, shift_y, rotation = getBugPosition()
	o.shift_x = shift_x
	o.shift_y = shift_y
	o = add_common_methods( o )

	o.x = x
	o.y = y
	--o.width = 65 * 1.8
	--o.height = 85 * 1.8
	o.rotation = rotation

	-- game settings
	o.power = 3
	o.life = 1
	o.score = 2
	o.speed = 3000
	o.splash_type = "green_yellow"

	return o
end

function _L.mushi(x, y)
	local imageSheetOptions = {
		width = 90 / 1.4,
		height = 205 / 1.4,
		numFrames = 6,
		sheetContentWidth = 540 / 1.4,
		sheetContentHeight = 205 / 1.4,
	}

	local imageSheet = graphics.newImageSheet( 'images/bugs/denkimushi.png', imageSheetOptions )

	local o = display.newSprite( imageSheet, {
		{name = 'eat', start = 1, count = 1},
		{name = 'walk', start = 1, count = 6, time = 500},
		{name = 'dead', start = 1, count = 1},
	} )

	local x, y, shift_x, shift_y, rotation = getBugPosition()
	o.shift_x = shift_x
	o.shift_y = shift_y
	o = add_common_methods( o )

	o.x = x
	o.y = y
	o.rotation = rotation

	-- game settings
	o.power = 4
	o.life = 10
	o.score = 15
	o.speed = 6000
	o.shake = true
	o.splash_type = "pink"

	return o
end

return _L