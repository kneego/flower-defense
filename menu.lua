local composer = require( "composer" )
local scene = composer.newScene()

local physics = require( "physics" )
local widget = require( "widget" )

local bug = require( 'bug' )
local common = require( "common" )

local bg
local audioSwitch
local bug_timer
local bug_on_menu

function scene:create( event )
	utils.debug('menu:scene:create')

	-- initialize the scene
	local sceneGroup = self.view

	bg = common.background()
	sceneGroup:insert( bg )

	local bug_bg = display.newImageRect( sceneGroup, 'images/bg/bug.png', screenW * display_ar, screenH )
	bug_bg.x = halfW
	bug_bg.y = halfH

	-- play button
	local play_group = display.newGroup( )
	sceneGroup:insert( play_group )

	local play_button = display.newImageRect( play_group, 'images/button.png', 120, 120 )
	play_button.x = halfW
	play_button.y = screenH - 20
	play_button.anchorY = 1

	local play_label = display.newEmbossedText( {
		text = R.strings('play_label'),
		font = 'GoodDogPlain',
		fontSize = 30,
		x = halfW,
		y = halfH
	} )
	play_label:setEmbossColor( common.EMBOSS_COLOR )
	play_label.x = halfW
	play_label.y = play_button.y - 60
	play_group:insert( play_label )

	utils.handlerAdd( play_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( play_group )
		elseif event.phase == 'ended' and common.button_up( play_group ) then
			if settings.show_help then
				composer.gotoScene( 'help' )
			else
				composer.gotoScene( 'level_selection' )
			end
		end

		return true
	end )

	-- settings button
	local settings_button = display.newImageRect( play_group, 'images/settings.png', 60, 60 )
	settings_button.x = halfW + 100
	settings_button.y = screenH - 20
	settings_button.anchorY = 1
	sceneGroup:insert( settings_button )

	utils.handlerAdd( settings_button, 'touch', function( self, event )
		if event.phase == 'began' then
			common.button_down( settings_button )
		elseif event.phase == 'ended' and common.button_up( settings_button ) then
			composer.gotoScene( 'game_settings' )
		end

		return true
	end )

	-- leader board button
	local leaderBoard = common.leaderBoardIcon()
	sceneGroup:insert( leaderBoard )

	-- leader board switch
	utils.handlerAdd( leaderBoard, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( leaderBoard )
		elseif event.phase == 'ended' and common.button_up( leaderBoard ) then
			game_network.show_leaderboards()
		end

		return true
	end )

	-- add FB and twitter buttons
	common.social_networks_buttons( sceneGroup )
end

function scene:show( event )
	if event.phase == 'will' then
		bug_timer = timer.performWithDelay( 2000, function()
			-- 50% skip
			if math.random( 2 ) == 1 then return end

			-- create bug
			bug_on_menu = bug.bug_on_menu()
			bug_on_menu:setSequence('walk')
			bug_on_menu:play( )

			bug_on_menu.move()
		end, -1)
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- pause the scene (stop timers, stop animation, unload sounds, etc.)

	elseif phase == "did" then
		timer.cancel( bug_timer )
		display.remove( bug_on_menu )
	end	
	
end

function scene:destroy( event )
	local sceneGroup = self.view

end

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene